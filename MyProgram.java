import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Scanner;
import java.lang.StringBuilder;

public class MyProgram {

	// Create parser object
	static MyParser parser = new MyParser();

	static ArrayList<Character> alist;

	// Variable array.
	static List<String> variables = new ArrayList<String>(
			Arrays.asList("S", "L", "L'", "I", "A", "C", "O", "W", "E", "E'", "E2", "E2'", "T", "Op1", "Op2"));

	// Terminal array.
	static List<String> terminals = new ArrayList<String>(Arrays.asList("uid", ":=", ";", "if", "then", "fi", "else",
			"while", "do", "done", "c", "<", "=", "!=", "+", "-"));

	// Main method.
	public static void main(String[] args) {

		// Boolean value, true if error mode is enabled by '-e' flag.
		boolean isErrorMode = false;

		if (args.length == 2) {
			if (args[1].equals("-e")) {
				isErrorMode = true;
			}
		}

		// Try and create a File object from path specified in first
		// command-line argument.
		File file;
		String input = null;
		try {
			file = new File(args[0]);
			Scanner scan = new Scanner(file);
			scan.useDelimiter("\\Z");
			input = scan.next();
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
			System.exit(0);
		}

		// Remove whitespace from string.
		char charArray[] = input.toCharArray();
		alist = new ArrayList<Character>();
		for (char charac : charArray) {
			if (!Character.isWhitespace(charac)) {
				alist.add(charac);
			}

		}

		// Begin to parse the string.

		// Print string to be parsed:
		System.out.print("Parsing ");
		for (char c : alist) {
			System.out.print(c);
		}
		System.out.print("\n");

		// Appending "$" to input string to signal end of string.
		alist.add('$');

		// Place the start variable on the stack and scan the first token.
		parser.pushStack("$");
		parser.pushStack("S"); // Assuming S is the start variable.

		int i = 0;
		while (true) {

			System.out.print("Input string remaining: ");
			for (int j = i; j < alist.size(); j++) {
				System.out.print(alist.get(j));
			}
			for (int k = 0; k < i; k++) {
				System.out.print(" ");
			}
			System.out.print("\t");
			System.out.print("Stack: ");
			System.out.println(parser.printStack());

			// Get token.
			StringBuilder tempString = new StringBuilder();
			String token = null;
			int j = 0;
			for (j = i; j < alist.size(); j++) {
				tempString.append(alist.get(j));
				token = tempString.toString();
				if (terminals.contains(token)) {
					// Checking the annoying case of do and done causing errors
					if (alist.get((j + 1)) == 'n') {
						continue;
					}
					break;
				}
			}
			/*
			 * If top of stack is a variable, pop stack and push string given in
			 * table for entry T[variable, currentToken]. If the given is empty,
			 * reject input string.
			 */
			if (variables.contains(parser.peekStack())) {
				String newString = parser.getProductionRule(token, parser.peekStack());
				if (newString == null) {
					if (isErrorMode) {
						recover(token, i);
						continue;
					}
					System.out.print("Error: expected a symbol from [");
					ArrayList<String> possibles = new ArrayList<String>();
					for (String terminal : terminals) {
						if (parser.getProductionRule(terminal, parser.peekStack()) != null) {
							possibles.add(terminal);
						}
					}
					if (possibles.size() > 0) {
						System.out.print(possibles.get(0));
						for (int k = 1; k < possibles.size(); k++) {
							System.out.print(", " + possibles.get(k));
						}
					}
					System.out.println("] instead of \"" + token + "\".");
					reject();
				} else {
					parser.popStack();
					if (!newString.equals("")) {
						parser.pushStack(newString);
					}
					continue;
				}

			}

			/*
			 * If top of stack is terminal, compare current token to top of
			 * stack. If match, pop stack, get next token, begin loop again. If
			 * not, reject input string.
			 */
			if (terminals.contains(parser.peekStack())) {
				if (token.equals(parser.peekStack())) {
					parser.popStack();
					i = j + 1;
					continue;
				} else {
					if (isErrorMode) {
						recover(token, i);
						continue;
					}
					System.out.println(
							"Error: expected symbol \"" + parser.peekStack() + "\" instead of \"" + token + "\".");
					reject();
				}
			}

			/*
			 * If current token is $, accept input. Stack should be empty ($).
			 */
			if (token.equals("$")) {
				if (parser.peekStack().equals("$")) {
					accept();
				} else {
					System.out.println("Error: reached end of input unexpectedly.");
					reject();
				}
			}
		}
	}

	// Function to print REJECTED and exit the program.
	public static void reject() {
		System.out.println("REJECTED");
		System.exit(0);
	}

	// Function to print ACCEPTED and exit the program.
	public static void accept() {
		System.out.println("ACCEPTED");
		System.exit(0);
	}

	// Error recovery method.
	public static void recover(String token, int i) {
		System.out.println("Warning: an error occured during parsing.");
		System.out.println("Attempting to recover...");
		// If stack has terminal, add terminal to input.
		if (terminals.contains(parser.peekStack())) {
			System.out.println("Adding \"" + parser.peekStack() + "\" to input.");
			for (int l = parser.peekStack().length() - 1; l >= 0; l--) {
				alist.add(i, parser.peekStack().charAt(l));
			}
			System.out.println("Continuing parsing:");
			return;
		}
		// If stack has variable, add a valid string to input.
		if (variables.contains(parser.peekStack())) {
			for (String terminal : terminals) {
				if (parser.getProductionRule(terminal, parser.peekStack()) != null) {
					System.out.println("Adding \"" + terminal + "\" to input.");
					for (int l = terminal.length() - 1; l >= 0; l--) {
						alist.add(i, terminal.charAt(l));
					}
					System.out.println("Continuing parsing:");
					return;
				}
			}
		}

	}

}
