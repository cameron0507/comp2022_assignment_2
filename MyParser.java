import java.util.HashMap;
import java.util.Stack;
import java.lang.StringBuilder;

public class MyParser {
	// Variables.
	private Stack<String> stack;
	private String[][] table = new String[17][15];
	private HashMap<String, Integer> variableMap = new HashMap<String, Integer>();
	private HashMap<String, Integer> terminalMap = new HashMap<String, Integer>();

	// Constructor.
	public MyParser() {

		// Initialize an empty stack.
		stack = new Stack<String>();

		// Populate the HashMaps

		// variableMap:
		variableMap.put("S", 0);
		variableMap.put("L", 1);
		variableMap.put("L'", 2);
		variableMap.put("I", 3);
		variableMap.put("A", 4);
		variableMap.put("C", 5);
		variableMap.put("O", 6);
		variableMap.put("W", 7);
		variableMap.put("E", 8);
		variableMap.put("E'", 9);
		variableMap.put("E2", 10);
		variableMap.put("E2'", 11);
		variableMap.put("T", 12);
		variableMap.put("Op1", 13);
		variableMap.put("Op2", 14);

		// terminalMap:
		terminalMap.put("$", 0);
		terminalMap.put("uid", 1);
		terminalMap.put("=:", 2);
		terminalMap.put(";", 3);
		terminalMap.put("if", 4);
		terminalMap.put("then", 5);
		terminalMap.put("fi", 6);
		terminalMap.put("else", 7);
		terminalMap.put("while", 8);
		terminalMap.put("do", 9);
		terminalMap.put("done", 10);
		terminalMap.put("c", 11);
		terminalMap.put("<", 12);
		terminalMap.put("=", 13);
		terminalMap.put("!=", 14);
		terminalMap.put("+", 15);
		terminalMap.put("-", 16);

		// Create new 2D String array, fill with parse table stuff.
		// table[terminals][variables]
		// S
		table[1][0] = "L";
		table[4][0] = "L";
		table[8][0] = "L";

		// L
		table[1][1] = "I L'";
		table[4][1] = "I L'";
		table[8][1] = "I L'";

		// L'
		table[0][2] = "";
		table[1][2] = "L";
		table[4][2] = "L";
		table[6][2] = "";
		table[7][2] = "";
		table[8][2] = "L";
		table[10][2] = "";

		// I
		table[1][3] = "A";
		table[4][3] = "C";
		table[8][3] = "W";

		// A
		table[1][4] = "uid := E ;";

		// C
		table[4][5] = "if E then L O fi";

		// O
		table[6][6] = "";
		table[7][6] = "else L";

		// W
		table[8][7] = "while E do L done";

		// E
		table[1][8] = "E2 E'";
		table[11][8] = "E2 E'";

		// E'
		table[3][9] = "";
		table[5][9] = "";
		table[9][9] = "";
		table[12][9] = "Op1 E2 E'";
		table[13][9] = "Op1 E2 E'";
		table[14][9] = "Op1 E2 E'";

		// E2
		table[1][10] = "T E2'";
		table[11][10] = "T E2'";

		// E2'
		table[3][11] = "";
		table[5][11] = "";
		table[9][11] = "";
		table[12][11] = "";
		table[13][11] = "";
		table[14][11] = "";
		table[15][11] = "Op2 E2";
		table[16][11] = "Op2 E2";

		// T
		table[1][12] = "uid";
		table[11][12] = "c";

		// Op1
		table[12][13] = "<";
		table[13][13] = "=";
		table[14][13] = "!=";

		// Op2
		table[15][14] = "+";
		table[16][14] = "-";

	}

	// Methods.

	// Method to get the production rule to follow
	public String getProductionRule(String terminal, String variable) {
		try {
			return table[terminalMap.get(terminal)][variableMap.get(variable)];
		} catch (NullPointerException e) {
			return null;
		}
	}

	// Stack methods
	public String peekStack() {
		return stack.peek();
	}

	public String popStack() {
		return stack.pop();
	}

	public void pushStack(String string) {
		String array[] = string.split(" ");
		for (int i = array.length - 1; i >= 0; i--) {
			stack.push(array[i]);
		}
	}

	public String printStack() {
		StringBuilder string = new StringBuilder();
		Stack<String> temp = new Stack<String>();
		while (!stack.empty()) {
			string.append(stack.peek());
			temp.push(stack.pop());
		}
		while (!temp.empty()) {
			stack.push(temp.pop());
		}
		return string.toString();
	}

}
